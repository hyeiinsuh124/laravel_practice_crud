@extends('layouts.app')

@section('content')
    <div class="container"> 
        <div class="row">   
            <div class="postAdd col-lg-12 text-right" style="margin-bottom: 20px">    
                <a href="{{url('add-client')}}" class="btn btn-primary">Add Client</a>
            </div>
        </div>
        <client-component></client-component>
    </div>
@endsection