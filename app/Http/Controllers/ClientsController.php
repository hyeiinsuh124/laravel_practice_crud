<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClientsController extends Controller
{
    //
    public function index(){
        return view('clientList');
    }

    public function addClient(){
        return view('add-client');
    }
}
