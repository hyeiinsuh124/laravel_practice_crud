<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('clientList', 'ClientsController@index') -> name('clientList');
Route::get('add-client', 'ClientsController@addClient') -> name('add-client');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');