<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Clients::class, function (Faker $faker) {
    return [
        //
            'name' => $faker ->name,
            'address' => $faker ->sentence($nbWords = 5),
            'email' => $faker ->safeEmail,
            'phone' => $faker -> randomNumber  
    ];
});
